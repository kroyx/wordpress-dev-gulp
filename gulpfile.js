const {task, src, dest, watch, series, parallel} = require('gulp'),
      plumber = require('gulp-plumber'),
      sass = require('gulp-sass'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      concat = require('gulp-concat'),
      sourcemaps = require('gulp-sourcemaps'),
      minify = require('gulp-minify'),
      imagemin = require('gulp-imagemin'),
      lineEndingCorrector = require('gulp-line-ending-corrector'),
      browserSync = require('browser-sync').create(),
      reload = browserSync.reload;

// Nombre theme de worpdress
const themeName = 'prueba';

// Ruta raiz del tema
const root = `../${themeName}`;

// Ruta de los directorios de desarrollo
const dev = {
  scss: `${root}/dev/scss`,
  js: `${root}/dev/js`,
  img: `${root}/dev/img`
};

// Ruta de destino de las tareas de gulp
const public = {
  css: `${root}/css`,
  js: `${root}/js`,
  img: `${root}/img`,
};

// Ruta de los archivos que seran utilizados para el watch
const watchs = {
  scss: `${dev.scss}/**/*.scss`,
  js: `${dev.js}/**/*.js`,
  php: `${root}/**/*.php`,
};

// lista en orden de archivos Js que seran concatenados
const jsSrc = [
  `${dev.js}/func/*.js`,
  `${dev.js}/components/*.js`,
  `${dev.js}/app.js`
];

const scss = () => src(`${dev.scss}/style.scss`)
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(plumber())
  .pipe(sass({
    outputStyle: 'compact',
  }).on('error', sass.logError))
  .pipe(postcss([
    autoprefixer({
      browsers: '>1%, last 2 versions'
    })
  ]))
  .pipe(sourcemaps.write())
  .pipe(lineEndingCorrector())
  .pipe(dest(`${public.css}/`))
  .pipe(reload({stream: true}));

const js = () => src(jsSrc)
  .pipe(plumber())
  .pipe(concat('app.js'))
  .pipe(lineEndingCorrector())
  .pipe(minify())
  .pipe(dest(`${public.js}/`))
  .pipe(reload({stream: true}));

const img = () => src(`${dev.img}/**/**`)
  .pipe(imagemin([
    imageMin.gifsicle({interlaced: true}),
    imageMin.jpegtran({progressive: true}),
    imageMin.optipng({optimizationLevel: 5}),
    imageMin.svgo({
      plugins:[
        {removeViewBox: false}
      ]
    })
  ]))
  .pipe(dest(`${public.img}`))
  .pipe(reload({stream: true}));

const server = () => {
  browserSync.init({
    open: 'external',
    proxy: 'http://localhost/wordpress',
    port: 8080,
  })
  watch(watchs.scss, scss);
  watch(watchs.js, js);
  watch(watchs.php).on('change', reload);
}

exports.scss= scss;
exports.js = js;
exports.img = img;
exports.server = server;

task('default', parallel(server));